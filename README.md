# nexus-demo

Spring Boot demo project for Nexus

## Getting started

- Starting _Nexus_ container
```
docker-compose up -d     (-d stands for detached mode)
```
- _Nexus_ will be running on _http://localhost:8081_
- Username: _admin_
- To get the default generated password for _admin_ use the command below
```
docker exec -it nexus cat /nexus-data/admin.password
```
- Add the code below to your _settings.xml_ in _.m2_ directory
```
        <server>
            <id>nexus-demo</id>
            <username>admin</username>
            <password>Aa123456</password>
        </server>
```
We use the _admin_ user just for the demonstration, but You can easily create users on _http://localhost:8081/#admin/security/users_
- Then edit your pom.xml file and the lines below. This will allow you to publish your project to nexus.
```
	<distributionManagement>
		<snapshotRepository>
			<id>nexus-demo</id>
			<name>nexus-snapshot</name>
			<url>http://localhost:8081/repository/maven-snapshots</url>
		</snapshotRepository>
		<repository>
			<id>nexus-demo</id>
			<name>nexus-release</name>
			<url>http://localhost:8081/repository/maven-releases</url>
		</repository>
	</distributionManagement>
```
- To publish your project use the command below (without running tests)
```
mvn clean deploy -DskipTests
```